FROM ubuntu:16.04
MAINTAINER Mark McCahill <mccahill@duke.edu>

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

# use local repositories 
RUN sed -i 's/archive.ubuntu.com/archive.linux.duke.edu/' /etc/apt/sources.list
RUN sed -i 's/security.ubuntu.com/archive.linux.duke.edu/' /etc/apt/sources.list

RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates \
    apt-utils

RUN apt-get update   &&  apt-get dist-upgrade -y

RUN apt-get install -y --force-yes --no-install-recommends \
        python-numpy \ 
        software-properties-common \
        wget \
        curl \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        vim-common \
        vim \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        x11vnc \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        xserver-xorg-video-dummy \
        xarchiver \
        gzip \
        unzip \
        locales \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

ADD fix-permissions /usr/local/bin/fix-permissions

# create an ubuntu user
#PASS=`pwgen -c -n -1 10`
#PASS=ubuntu
#echo "User: ubuntu Pass: $PASS"
#RUN useradd --create-home --shell /bin/bash --user-group --groups adm,sudo ubuntu

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd


ADD startup.sh /
ADD cleanup-cruft.sh /
ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
ADD noVNC /noVNC/

# make sure the noVNC self.pem cert file is only readable by root
RUN chmod 400 /noVNC/self.pem

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf


RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 python-pip \
 python-tk \
 python-numpy \
 python-scipy \
 python-matplotlib \
 git \
 python3-pip \
 python3-tk \
 python3-numpy \
 python3-scipy \
 python3-matplotlib

RUN  pip install pillow
RUN  pip3 install pillow


############ begin Liberica JDK version 12.0.2  ###############
# 
#RUN wget https://download.bell-sw.com/java/12.0.2/bellsoft-jdk12.0.2-linux-amd64.deb
#RUN dpkg -i bellsoft-jdk12.0.2-linux-amd64.deb
#
############ end Liberica JDK version 12.0.2 ###############

################# begin cisTEM ##################
#
RUN cd /usr/local ; \
    wget 'https://cistem.org/system/tdf/upload3/cistem-1.0.0-beta-intel-linux.tar.gz?file=1&type=cistem_details&id=37&force=0&s3fs=1' \
         -O cistem-1.0.0-beta-intel-linux.tar.gz ; \
    tar -zxf cistem-1.0.0-beta-intel-linux.tar.gz ; \
    rm  /usr/local/cistem-1.0.0-beta-intel-linux.tar.gz
	
################# end cisTEM ####################

# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6

########### conda

#ENV NBUSER=ubuntu
## Configure environment
#ENV CONDA_DIR=/opt/conda \
#    SHELL=/bin/bash \
#    NB_USER=$NB_USER \
#    NB_UID=$NB_UID \
#    NB_GID=$NB_GID \
#    LC_ALL=en_US.UTF-8 \
#    LANG=en_US.UTF-8 \
#    LANGUAGE=en_US.UTF-8
#ENV PATH=$CONDA_DIR/bin:$PATH \
#    HOME=/home/$NB_USER
#
## Install conda and check the md5 sum provided on the download site
#ENV MINICONDA_VERSION=4.6.14 \
#    CONDA_VERSION=4.7.10
#RUN cd /tmp && \
#    wget --quiet https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh && \
#    echo "718259965f234088d785cad1fbd7de03 *Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh" | md5sum -c - && \
#    /bin/bash Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
#    rm Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh && \
#    $CONDA_DIR/bin/conda config --system --prepend channels conda-forge && \
#    $CONDA_DIR/bin/conda config --system --set auto_update_conda false && \
#    $CONDA_DIR/bin/conda config --system --set show_channel_urls true && \
#    $CONDA_DIR/bin/conda install --quiet --yes conda="${MINICONDA_VERSION%.*}.*" && \
#    $CONDA_DIR/bin/conda update --all --quiet --yes && \
#    $CONDA_DIR/bin/conda clean -tipsy && \
#    rm -rf /home/$NB_USER/.cache/yarn 
##    fix-permissions $CONDA_DIR && \
##    fix-permissions /home/$NB_USER
#
#RUN conda install --quiet --yes -c conda-forge nodejs
## Install Jupyter Notebook, Lab, and Hub
## Generate a notebook server config
## Cleanup temporary files
## Correct permissions
## Do all this in a single RUN command to avoid duplicating all of the
## files across image layers when the permissions change
#RUN $CONDA_DIR/bin/conda install --quiet --yes \
#    'notebook=5.7.0' \
#    jupyterlab=0.35 \ 
#    jupyterhub \ 
#    "ipywidgets>=7.2" && \
#    $CONDA_DIR/bin/conda clean -tipsy && \
##    jupyter labextension install @jupyterlab/hub-extension@^0.11.0 && \
#    npm cache clean --force && \
#    jupyter notebook --generate-config && \
#    rm -rf $CONDA_DIR/share/jupyter/lab/staging && \
#    rm -rf /home/$NB_USER/.cache/yarn 
#    /usr/local/bin/fix-permissions $CONDA_DIR && \
#    /usr/local/bin/fix-permissions /home/$NB_USER



ENTRYPOINT ["/startup.sh"]
